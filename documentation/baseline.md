---
title: Onboarded baseline trees
---

## [arm-acpi](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=arm-acpi)

Run tests on arm-acpi kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/lpieralisi/linux.git>
- branches: acpi/for-next
- responsible: <lorenzo.pieralisi@arm.com>
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/merge_requests/64>
- notifications: test maintainer and reponsible contact (see above)
- test set: `acpi`
- architectures: `aarch64`

## [arm-next](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=arm-next)

Run tests on arm-next kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/arm64/linux.git>
- branches: for-kernelci
- responsible: <will@kernel.org>, <catalin.marinas@arm.com>, <linux-arm-kernel@lists.infradead.org>
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/merge_requests/19>
- notifications: test maintainer and reponsible contact (see above)
- test set: `kt1|arm-upstream`
- architectures: `aarch64`

## [block](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=block)

Run tests on block kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/axboe/linux-block.git>
- branches: for-next, for-current
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/merge_requests/41>
- notifications: test maintainer
- test set: `stor|fs`
- architectures: `x86_64 ppc64le aarch64 s390x`

## [gfs2](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=gfs2)

Run tests on gfs2 kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/gfs2/linux-gfs2.git>
- branches: for-next
- responsible: <gfs2-maint@redhat.com>
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/merge_requests/135>
- notifications: test maintainer and reponsible contact (see above)
- test set: `gfs2`
- architectures: `x86_64 ppc64le aarch64 s390x`

## [kvm](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=kvm)

Run tests on kvm kernel tree.

- repo: <https://git.kernel.org/pub/scm/virt/kvm/kvm.git>
- branches: master, next
- responsible: <jarichte@redhat.com>
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/merge_requests/77>
- notifications: test maintainer and reponsible contact (see above)
- test set: `virt`
- architectures: `x86_64`

## [mainline.kernel.org](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=mainline.kernel.org)

Run tests on mainline.kernel.org kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git>
- branches: master
- responsible: <nathan@kernel.org>
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/merge_requests/1/>
- notifications: test maintainer and reponsible contact (see above)
- test set: `kt1|kself`
- architectures: `x86_64 ppc64le aarch64 s390x`

## [mainline.kernel.org-clang](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=mainline.kernel.org-clang)

Run tests on mainline.kernel.org-clang kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git>
- branches: master
- responsible: <llvm@lists.linux.dev>
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/merge_requests/91>
- notifications: test maintainer and reponsible contact (see above)
- test set: `kt1|kself`
- architectures: `x86_64 ppc64le aarch64 s390x`

## [nf](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=nf)

Run tests on nf kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/netfilter/nf.git>
- branches: main
- responsible: <egarver@redhat.com>, <fwestpha@redhat.com>, <psutter@redhat.com>, <yiche@redhat.com>
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/issues/24>
- notifications: test maintainer and reponsible contact (see above)
- test set: `netfilter-upstream`
- architectures: `x86_64`

## [nf-next](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=nf-next)

Run tests on nf-next kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/netfilter/nf-next.git>
- branches: main, testing
- responsible: <egarver@redhat.com>, <fwestpha@redhat.com>, <psutter@redhat.com>, <yiche@redhat.com>
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/issues/24>
- notifications: test maintainer and reponsible contact (see above)
- test set: `netfilter-upstream`
- architectures: `x86_64`

## [printk](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=printk)

Run kexec/kdump cases on printk tree to make sure atomic console doesn't cause any troubles.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/printk/linux.git>
- branches: for-next
- responsible: <bhe@redhat.com>
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/issues/14>
- notifications: test maintainer and reponsible contact (see above)
- test set: `kdump`
- architectures: `x86_64 aarch64`

## [rt-devel](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=rt-devel)

Run tests on rt-devel kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/rt/linux-rt-devel.git>
- branches: linux-6.3.y-rt
- responsible: <kernel-rt-ci@redhat.com>
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/merge_requests/14>
- notifications: test maintainer and reponsible contact (see above)
- test set: `kt1|kself`
- architectures: `x86_64`

## [rt-stable](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=rt-stable)

Run tests on rt-stable kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/rt/linux-stable-rt.git>
- branches: v6.1-rt, v5.15-rt, v5.10-rt
- responsible: <kernel-rt-ci@redhat.com>
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/issues/5>
- notifications: test maintainer and reponsible contact (see above)
- test set: `kt1|kself`
- architectures: `x86_64`

## [scsi](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=scsi)

Run tests on scsi kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/mkp/scsi.git>
- branches: for-next
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/merge_requests/1>
- notifications: test maintainer
- test set: `stor`
- architectures: `x86_64 ppc64le aarch64 s390x`

## [stable](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=stable)

Run tests on stable kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git>
- branches: linux-6.10.y
- responsible: <jforbes@redhat.com>
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/merge_requests/1>
- notifications: test maintainer and reponsible contact (see above)
- test set: `kt1`
- architectures: `x86_64 ppc64le aarch64 s390x`

## [stable-queue](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=stable-queue)

Run tests on stable-queue kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git>
- branches: queue/6.11
- responsible: <jforbes@redhat.com>
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/merge_requests/1>
- notifications: test maintainer and reponsible contact (see above)
- test set: `kt1`
- architectures: `x86_64 ppc64le aarch64 s390x`

## [tnguy-net](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=tnguy-net)

Run tests on tnguy-net kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/tnguy/net-queue.git>
- branches: dev-queue
- responsible: <zfang@redhat.com>
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/issues/7>
- notifications: test maintainer and reponsible contact (see above)
- test set: `net-ice`
- architectures: `x86_64`

## [tnguy-next](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=tnguy-next)

Run tests on tnguy-next kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/tnguy/next-queue.git>
- branches: dev-queue
- responsible: <zfang@redhat.com>
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/issues/7>
- notifications: test maintainer and reponsible contact (see above)
- test set: `net-ice`
- architectures: `x86_64`

## [xfs](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=xfs)

Run tests on xfs kernel tree.

- repo: <https://git.kernel.org/pub/scm/fs/xfs/xfs-linux.git>
- branches: for-next
- responsible: <cmaiolin@redhat.com>
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/issues/21>
- notifications: test maintainer and reponsible contact (see above)
- test set: `xfs-upstream`
- architectures: `x86_64 ppc64le aarch64 s390x`

## [xfsprogs](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=xfsprogs)

Run tests on xfsprogs tree.

- repo: <https://git.kernel.org/pub/scm/fs/xfs/xfs-linux.git>
- branches: for-next
- responsible: <cmaiolin@redhat.com>
- onboarding request: <https://gitlab.com/cki-project/pipeline-data/-/issues/21>
- notifications: test maintainer and reponsible contact (see above)
- test set: `xfs-upstream`
- architectures: `x86_64 ppc64le aarch64 s390x`
