---
title: Onboarded ofa trees
---

## [libfabric](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=libfabric)

Run tests on libfabric kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/rdma/rdma.git>
- branches: for-next
- responsible: <fsdpwg@lists.openfabrics.org>
- onboarding request: <https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db/-/merge_requests/904>
- notifications: test maintainer and reponsible contact (see above)
- test set: `ofa`
- tests regex: `OFA - LibFabric.*`
- architectures: `x86_64`

## [rdma](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=rdma)

Run tests on rdma kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/rdma/rdma.git>
- branches: for-next, for-rc, hmm, wip/jgg-for-rc, wip/jgg-for-next, wip/leon-for-rc, wip/leon-for-next, wip/for-testing
- responsible: <fsdpwg@lists.openfabrics.org>
- onboarding request: <https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db/-/merge_requests/904>
- notifications: test maintainer and reponsible contact (see above)
- test set: `ofa`
- tests regex: `OFA - Kernel RDMA.*`
- architectures: `x86_64`

## [rdma-core](https://datawarehouse.cki-project.org/kcidb/checkouts?filter_gittrees=rdma-core)

Run tests on rdma-core kernel tree.

- repo: <https://git.kernel.org/pub/scm/linux/kernel/git/rdma/rdma.git>
- branches: for-next
- responsible: <fsdpwg@lists.openfabrics.org>
- onboarding request: <https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db/-/merge_requests/904>
- notifications: test maintainer and reponsible contact (see above)
- test set: `ofa`
- tests regex: `OFA - Userspace RDMA.*`
- architectures: `x86_64`
