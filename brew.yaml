---
.report_rule_to_failed_test_maintainers: &report_rule_to_failed_test_maintainers
  when: failed_tests
  send_to: failed_tests_maintainers

.report_rule_to_submitter: &report_rule_to_submitter
  when: always
  send_to: submitter

.default:
  cki_pipeline_project: cki-internal-contributors
  test_set: 'kt1'

.developers:
  .test_scratch: 'True'
  test_priority: 'normal'
  .report_rules:
    - !reference [.report_rule_to_submitter]

# rhel
.rhel:
  package_name: kernel
  source_package_name: kernel
  architectures: 'x86_64 s390x ppc64le aarch64'
  send_pre_test_notification: 'True'
  .test_scratch: 'False'
  test_priority: 'high'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - !reference [.report_rule_to_submitter]

kernel-rhel6:
  .extends: [.rhel]
  kcidb_tree_name: rhel-6.10
  cki_pipeline_branch: rhel6-brew
  rpm_release: 'el6'
  architectures: 'i686 x86_64 s390x'

.rhel7:
  .extends: [.rhel]
  cki_pipeline_branch: rhel7-brew
  rpm_release: 'el7'
  architectures: 'x86_64 s390x ppc64le ppc64'

kernel-rhel76:
  .extends: [.rhel7]
  kcidb_tree_name: rhel-7.6
  rpm_release: '957\..*\.el7'

kernel-rhel76-developers:
  .extends: [.rhel7, .developers]
  kcidb_tree_name: rhel-7.6
  rpm_release: '957\..*\.el7.*\.test[.-]cki\.'

kernel-rhel77:
  .extends: [.rhel7]
  kcidb_tree_name: rhel-7.7
  rpm_release: '1062\..*\.el7'

kernel-rhel77-developers:
  .extends: [.rhel7, .developers]
  kcidb_tree_name: rhel-7.7
  rpm_release: '1062\..*\.el7.*\.test[.-]cki\.'

kernel-rhel79:
  .extends: [.rhel7]
  kcidb_tree_name: rhel-7.9
  rpm_release: '1160\..*\.el7'

kernel-rhel79-developers:
  .extends: [.rhel7, .developers]
  kcidb_tree_name: rhel-7.9
  rpm_release: '1160\..*\.el7.*\.test[.-]cki\.'

.rhel8:
  .extends: [.rhel]
  # kcidb_tree_name automatically determined by koji-trigger
  debug_architectures: 'x86_64'
  cki_pipeline_branch: rhel8-brew
  rpm_release: 'el8'

kernel-rhel8:
  .extends: [.rhel8]

kernel-rhel8-developers:
  .extends: [.rhel8, .developers]
  rpm_release: 'el8.*\.test[.-]cki\.'

.rhel9:
  .extends: [.rhel]
  # kcidb_tree_name automatically determined by koji-trigger
  debug_architectures: 'x86_64'
  cki_pipeline_branch: rhel9-brew
  .report_rules:
    # We need to copy all rules from .rhel as they are overriden, not appended
    - !reference [.report_rule_to_failed_test_maintainers]
    - !reference [.report_rule_to_submitter]
    - when: always
      send_to:
        - ptalbert@redhat.com

.rhel9_64k:
  .extends: [.rhel9]
  architectures: 'aarch64'
  package_name: 'kernel-64k'
  kpet_extra_components: '64kpages'

.rhel9_rt:
  .extends: [.rhel9]
  cki_pipeline_branch: rhel9-rt-brew
  architectures: 'x86_64 aarch64'
  package_name: 'kernel-rt'

.rhel9_rt_only_x86_64:
  .extends: [.rhel9_rt]
  architectures: 'x86_64'

.rhel9_rt_64k:
  .extends: [.rhel9]
  architectures: 'aarch64'
  package_name: 'kernel-rt-64k'
  kpet_extra_components: '64kpages'

.rhel9_release:
  rpm_release: 'el9_'  # no ystream

.rhel9_release_dev:
  rpm_release: 'el9.*\.test[.-]cki\.'

.rhel9_release_93_94_95:
  rpm_release: 'el9_[3-5]\b'

.rhel9_release_dev_93_94_95:
  rpm_release: 'el9_[3-5]\b.*\.test[.-]cki\.'

.rhel9_release_gte_96:
  rpm_release: 'el9(_[6-9]|_1[0-9])\b'  # no ystream

.rhel9_release_dev_gte_96:
  rpm_release: 'el9(|_[6-9]|_1[0-9])\b.*\.test[.-]cki\.'

kernel-rhel9:
  .extends: [.rhel9, .rhel9_release]

kernel-rhel9-developers:
  .extends: [.rhel9, .developers, .rhel9_release_dev]

kernel-rhel9-64k:
  .extends: [.rhel9_64k, .rhel9_release]

kernel-rhel9-64k-developers:
  .extends: [.rhel9_64k, .developers, .rhel9_release_dev]

kernel-rhel9-rt-93-94-95:
  .extends: [.rhel9_rt_only_x86_64, .rhel9_release_93_94_95]

kernel-rhel9-rt-developers-93-94-95:
  .extends: [.rhel9_rt_only_x86_64, .developers, .rhel9_release_dev_93_94_95]

kernel-rhel9-rt-gte-96:
  .extends: [.rhel9_rt, .rhel9_release_gte_96]

kernel-rhel9-rt-developers-gte-96:
  .extends: [.rhel9_rt, .developers, .rhel9_release_dev_gte_96]

kernel-rhel9-rt-64k:
  .extends: [.rhel9_rt_64k, .rhel9_release_gte_96]

kernel-rhel9-rt-64k-developers:
  .extends: [.rhel9_rt_64k, .developers, .rhel9_release_dev_gte_96]

.rhel10:
  .extends: [.rhel]
  # kcidb_tree_name automatically determined by koji-trigger
  cki_pipeline_branch: rhel10-brew
  rpm_release: 'el10'
  .report_rules:
    # We need to copy all rules from .rhel as they are overriden, not appended
    - !reference [.report_rule_to_failed_test_maintainers]
    - !reference [.report_rule_to_submitter]
    - when: always
      send_to:
        - ptalbert@redhat.com

.rhel10_debug:
  .extends: [.rhel10]
  package_name: 'kernel-debug'

.rhel10_64k:
  .extends: [.rhel10]
  architectures: 'aarch64'
  package_name: 'kernel-64k'
  kpet_extra_components: '64kpages'

.rhel10_64k_debug:
  .extends: [.rhel10_64k]
  package_name: 'kernel-64k-debug'

.rhel10_rt:
  .extends: [.rhel10]
  cki_pipeline_branch: rhel10-rt-brew
  architectures: 'x86_64 aarch64'
  package_name: 'kernel-rt'

.rhel10_rt_debug:
  .extends: [.rhel10_rt]
  package_name: 'kernel-rt-debug'

.rhel10_rt_64k:
  .extends: [.rhel10]
  architectures: 'aarch64'
  package_name: 'kernel-rt-64k'
  kpet_extra_components: '64kpages'

.rhel10_rt_64k_debug:
  .extends: [.rhel10_rt_64k]
  package_name: 'kernel-rt-64k-debug'

.rhel10_developers:
  .extends: [.developers]
  rpm_release: 'el10.*\.test[.-]cki\.'

kernel-rhel10-developers:
  .extends: [.rhel10, .rhel10_developers]

kernel-rhel10-debug-developers:
  .extends: [.rhel10_debug, .rhel10_developers]

kernel-rhel10-64k-developers:
  .extends: [.rhel10_64k, .rhel10_developers]

kernel-rhel10-64k-debug-developers:
  .extends: [.rhel10_64k_debug, .rhel10_developers]

kernel-rhel10-rt-developers:
  .extends: [.rhel10_rt, .rhel10_developers]

kernel-rhel10-rt-debug-developers:
  .extends: [.rhel10_rt_debug, .rhel10_developers]

kernel-rhel10-rt-64k-developers:
  .extends: [.rhel10_rt_64k, .rhel10_developers]

kernel-rhel10-rt-64k-debug-developers:
  .extends: [.rhel10_rt_64k_debug, .rhel10_developers]

kernel-automotive-rhel9:
  # kcidb_tree_name automatically determined by koji-trigger
  package_name: kernel-automotive
  source_package_name: kernel-automotive
  architectures: 'x86_64 aarch64'
  test_set: 'automotive'
  cki_pipeline_branch: rhel9-automotive-brew
  kpet_tree_name: rhivos
  rpm_release: 'el9iv'
  test_runner: aws

kernel-automotive-rhel9-tf:
  # kcidb_tree_name automatically determined by koji-trigger
  package_name: kernel-automotive
  source_package_name: kernel-automotive
  architectures: 'aarch64'
  debug_architectures: 'aarch64'
  test_set: 'automotive'
  cki_pipeline_branch: rhel9-automotive-brew
  kpet_tree_name: rhivos
  rpm_release: 'el9iv'
  test_runner: testingfarm
  AUTOMOTIVE_RELEASE: nightly
  TF_API_URL: https://api.dev.testing-farm.io/v0.1
  TF_POOL: ride4-cki
  TF_HARDWARE: 'ridesx4'

# kernel-rt
.kernel_rt:
  package_name: kernel-rt
  source_package_name: kernel-rt
  architectures: 'x86_64'
  send_pre_test_notification: 'False'
  .test_scratch: 'False'
  test_priority: 'high'
  .report_rules:
    - !reference [.report_rule_to_failed_test_maintainers]
    - !reference [.report_rule_to_submitter]
    - {when: always, send_to: kernel-rt-ci@redhat.com}

.kernel_rt_rhel7:
  .extends: [.kernel_rt]
  cki_pipeline_branch: rhel7-rt-brew

kernel-rt-rhel77:
  .extends: [.kernel_rt_rhel7]
  kcidb_tree_name: rhel-7.7
  rpm_release: '1062\..*\.el7'

kernel-rt-rhel79:
  .extends: [.kernel_rt_rhel7]
  kcidb_tree_name: rhel-7.9
  rpm_release: '1160\..*\.el7'

kernel-rt-rhel8:
  .extends: [.kernel_rt]
  # kcidb_tree_name automatically determined by koji-trigger
  debug_architectures: 'x86_64'
  cki_pipeline_branch: rhel8-rt-brew
  rpm_release: 'el8'

kernel-rt-rhel9:
  .extends: [.kernel_rt]
  # kcidb_tree_name automatically determined by koji-trigger
  debug_architectures: 'x86_64'
  cki_pipeline_branch: rhel9-rt-brew
  rpm_release: 'el9'
